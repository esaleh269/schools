package com.example.eslamsalah.newschool.Model;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.eslamsalah.newschool.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;


public class GcmRegistrationService extends IntentService {

    public static final String REGISTRATION_SUCCESS = "Regsuccess";
    public static final String REGISTRATION_ERROR = "Regerror";
    public GcmRegistrationService() {
        super("");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GcmRegist();

    }

    private void GcmRegist(){

        Intent registcomplete= null ;
        String token = null ;
        InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
        try {
            token = instanceID.getToken("627153428214", GoogleCloudMessaging.INSTANCE_ID_SCOPE ,null);

            Log.v("GcmRegService","token::"+token);
            Log.i("GcmRegService","token::"+token);
            Log.e("GcmRegService","token"+token);
            registcomplete = new Intent(GcmRegistrationService.REGISTRATION_SUCCESS);
            registcomplete.putExtra("token",token);
        } catch (IOException e) {
            e.printStackTrace();
            Log.v("GcmRegService","RegFaild");
            registcomplete = new Intent(GcmRegistrationService.REGISTRATION_ERROR);
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(registcomplete);

        SharedPreferences pref = getSharedPreferences("settoken",MODE_PRIVATE);
        SharedPreferences.Editor editor1 = pref.edit();
        editor1.putString("tokval",token);
        editor1.apply();

    }
}
