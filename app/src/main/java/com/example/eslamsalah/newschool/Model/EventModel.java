package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/23/2017.
 */

public class EventModel {

    private String name_en ;
    private String name_ar ;
    private String desc_en ;
    private String desc_ar ;
    private String date ;
    private String time ;
    private String millidate ;

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getDesc_en() {
        return desc_en;
    }

    public void setDesc_en(String desc_en) {
        this.desc_en = desc_en;
    }

    public String getDesc_ar() {
        return desc_ar;
    }

    public void setDesc_ar(String desc_ar) {
        this.desc_ar = desc_ar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMillidate() {
        return millidate;
    }

    public void setMillidate(String millidate) {
        this.millidate = millidate;
    }
}
