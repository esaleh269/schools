package com.example.eslamsalah.newschool.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Ch_Activities_Recycler;
import com.example.eslamsalah.newschool.Model.Ch_Act_Model;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Studentprofile extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,Ch_Activities_Recycler.Click {
    public static String Names[];
    public static int Pholtos[] = {R.drawable.hw, R.drawable.result, R.drawable.attendance,
            R.drawable.leaveperm, R.drawable.cal, R.drawable.gallery, R.drawable.lessontable,
            R.drawable.inbox2, R.drawable.increase
    };
        Intent intent ;
        RequestQueue requestQueue;
        ImageView userimg ;
        TextView username ;
        String img,name , photo ;
        ArrayList<Ch_Act_Model> Act_List ;
        RecyclerView chrecycler ;
        Ch_Activities_Recycler activities_recycler ;
        long [] Data ;
        Locale locale ;
        int studentid;
    Dialog myDialog;
    DrawerLayout drawer;
        android.support.v7.widget.Toolbar toolbar ;
    TextView studname, stage, grade, clas, department;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_studentprofile);
        intent = getIntent();
        toolbar=findViewById(R.id.student_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.student));
        toolbar.setTitleTextColor(Color.WHITE);
        Names = new String[]{getResources().getString(R.string.homework),getResources().getString(R.string.result),getResources().getString(R.string.absence),
                getResources().getString(R.string.leavepermission),getResources().getString(R.string.calendar),getResources().getString(R.string.gallery),
                getResources().getString(R.string.lessontable),getResources().getString(R.string.message),getResources().getString(R.string.financial)
        };

        GetStudent ();
        Set_Activities ();
        getdataa();


        Check_Parent_Exsistance();

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        myDialog = new Dialog(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    public void GetStudent (){

        final int uid = intent.getIntExtra("sid",0);
        String state = intent.getStringExtra("state");
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);
        userimg = toolbar.findViewById(R.id.student_img);
        username = toolbar.findViewById(R.id.student_name);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/login/getuser?id=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&uid=" + uid, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            name = response.getString("Name");
                            img = response.getString("Photo");
                            String xx = img.substring(2,img.length());


                            photo= dom+xx ;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        username.setText(name);
                        studentid=uid;
                        SharedPreferences pref = getSharedPreferences("info", MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = pref.edit();
                        editor1.putInt("studentid", studentid);
                        editor1.apply();
                        if (img=="null"){
                            userimg.setImageResource(R.drawable.icons8_person);
                        }
                        else {
                            Picasso.with(Studentprofile.this).load(photo).into(userimg);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);

    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.editprofile:
                Intent i8 = new Intent(Studentprofile.this, Edit_Profile.class);
                i8.putExtra("id", studentid);
                startActivity(i8);
                drawer.closeDrawers();
                break;
            case R.id.complaint:
                if (Check_Parent_Exsistance()) {
                    Intent intent4 = new Intent(Studentprofile.this, Complaint.class);
                    intent4.putExtra("studentid", studentid);
                    startActivity(intent4);
                } else {
                    Toast.makeText(this, "You Must Login As A Parent", Toast.LENGTH_SHORT).show();
                }
                drawer.closeDrawers();
                break;
            case R.id.votingg:
                Intent intent5 = new Intent(Studentprofile.this, Voting.class);
                intent5.putExtra("studentid", studentid);
                startActivity(intent5);
                drawer.closeDrawers();
                break;
            case R.id.det:
                showpopupdetails();
                drawer.closeDrawers();
                break;
            case R.id.logout:
                SharedPreferences pref = getSharedPreferences("info", MODE_PRIVATE);
                SharedPreferences.Editor editor1 = pref.edit();
                editor1.putInt("UserID", 0);
                editor1.apply();
                Intent i9 = new Intent(Studentprofile.this, Home.class);
                startActivity(i9);
                drawer.closeDrawers();
                break;

        }
//        int id = item.getItemId();
//
//        if (id == R.id.editprofile) {
//            Intent i8 = new Intent(Studentprofile.this,Edit_Profile.class);
//            i8.putExtra("id",id);
//            startActivity(i8);
//            // Handle the camera action
//        } else if (id == R.id.logout) {
//            SharedPreferences pref = getSharedPreferences("info",MODE_PRIVATE);
//            SharedPreferences.Editor editor1 = pref.edit() ;
//            editor1.putInt("UserID",0);
//            editor1.apply();
//            Intent i9 = new Intent(Studentprofile.this,Home.class);
//            startActivity(i9);
//        } else if (id == R.id.complaint) {
//            Intent intent4 = new Intent(Studentprofile.this,Complaint.class);
//            intent4.putExtra("studentid",studentid);
//            startActivity(intent4);
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void Set_Activities (){

        Act_List = new ArrayList<>(9);

        for (int i=0 ; i<9;i++){
            Ch_Act_Model ch_act_model = new Ch_Act_Model();
            ch_act_model.setName(Names[i]);
            ch_act_model.setImg(Pholtos[i]);

            Act_List.add(ch_act_model);
        }

        chrecycler = findViewById(R.id.student_rec);
        activities_recycler = new Ch_Activities_Recycler(Act_List,Studentprofile.this);
        activities_recycler.SetListner(Studentprofile.this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(Studentprofile.this,3);
        chrecycler.setLayoutManager(gridLayoutManager);
        chrecycler.setItemAnimator(new DefaultItemAnimator());
        chrecycler.setAdapter(activities_recycler);

    }

    public void getdataa(){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/Events?id=" + com.example.eslamsalah.newschool.Model.Data.school_id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Data =  new long[response.length()];

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);

                                String x = jsonObject.getString("Totalmilliseconds");
                                long a = Long.parseLong(x) ;
                                Data[i]=a ;


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Studentprofile.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);



    }

    @Override
    public void On_View_Click(int position) {


        final int id = intent.getIntExtra("sid",0);
        switch (position){
            case 0 :
                Intent i1 = new Intent(Studentprofile.this,Home_Work.class);
                i1.putExtra("id",id);
                startActivity(i1);
                break;
            case 1 :
                Intent i2 = new Intent(Studentprofile.this,Results.class);
                i2.putExtra("id",id);
                startActivity(i2);
                break;
            case 2 :
                Intent i3 = new Intent(Studentprofile.this,Absence.class);
                i3.putExtra("id",id);
                startActivity(i3);
                break;
            case 3 :
                Intent i4 = new Intent(Studentprofile.this,Leave_Permission_Report.class);
                i4.putExtra("id",id);
                startActivity(i4);
                break;
            case 4 :
                Intent i5 = new Intent(Studentprofile.this,Calendarr.class);
                i5.putExtra("id",id);
                Bundle bundle = new Bundle();
                bundle.putLongArray("data", Data);
                i5.putExtras(bundle);
                startActivity(i5);
                break;
            case 5 :
                Intent i6 = new Intent(Studentprofile.this,Gallery.class);
                i6.putExtra("id",id);
                startActivity(i6);
                break;
            case 6 :
                Intent i7 = new Intent(Studentprofile.this,Lesson_Table.class);
                i7.putExtra("id",id);
                startActivity(i7);
                break;
            case 7 :
                if (Check_Parent_Exsistance()) {
                    Intent i8 = new Intent(Studentprofile.this, Studentmessages.class);
                    i8.putExtra("id", id);
                    startActivity(i8);
                } else {
                    Toast.makeText(this, "You Must Login As A Parent", Toast.LENGTH_SHORT).show();
                }

                break;
            case 8 :
                if (Check_Parent_Exsistance()) {
                    Intent i9 = new Intent(Studentprofile.this, Accountstatement.class);
                    i9.putExtra("id", id);
                    startActivity(i9);
                } else {
                    Toast.makeText(this, "You Must Login As A Parent", Toast.LENGTH_SHORT).show();

                }

                break;
        }
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }
    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
    public void showpopupdetails(){
        TextView txtclose;

        myDialog.setContentView(R.layout.custompopup);
        txtclose = myDialog.findViewById(R.id.txtclose);
        studname = myDialog.findViewById(R.id.cont_name);
        stage = myDialog.findViewById(R.id.stage);
        grade = myDialog.findViewById(R.id.grade);
        clas = myDialog.findViewById(R.id.clas);
        department = myDialog.findViewById(R.id.depart);

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
        Getstudentdetails();
    }

    public void Getstudentdetails() {
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Student/Getby?schoolId=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&StudentUserId=" + studentid, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            studname.setText(response.getString("StudentName").toString());
                            stage.setText(response.getString("Phase").toString());
                            grade.setText(response.getString("SubPhase").toString());
                            clas.setText(response.getString("Class").toString());
                            department.setText(response.getString("Department").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Studentprofile.this, "get parent ::" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);

    }

    public boolean Check_Parent_Exsistance() {
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        String userType = preferences.getString("UserType", "kkk");

        return userType.equals("Parent");
    }
}
