package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/16/2017.
 */

public class HomeWork_Model {

    private String qtitle ;
    private String qdesc ;
    private String qid ;

    public String getQtitle() {
        return qtitle;
    }

    public void setQtitle(String qtitle) {
        this.qtitle = qtitle;
    }

    public String getQdesc() {
        return qdesc;
    }

    public void setQdesc(String qdesc) {
        this.qdesc = qdesc;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }
}
