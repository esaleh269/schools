package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.Ch_Act_Model;
import com.example.eslamsalah.newschool.R;

import java.util.ArrayList;

/**
 * Created by eslam.salah on 12/14/2017.
 */

public class Ch_Activities_Recycler extends RecyclerView.Adapter<Ch_Activities_Recycler.VHolder> {

    ArrayList<Ch_Act_Model> List ;
    Context context ;
    Click click ;

    public Ch_Activities_Recycler (ArrayList<Ch_Act_Model> List1 ,Context context1){
        this.List = List1 ;
        this.context=context1 ;

    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activities_view_shape,parent,false);
        return new Ch_Activities_Recycler.VHolder(view);

    }

    public void SetListner (Click click1){
        click=click1 ;
    }
    @Override
    public void onBindViewHolder(VHolder holder, final int position) {

        Ch_Act_Model ch_act_model = List.get(position);
        holder.img.setImageResource(ch_act_model.getImg());
        holder.txt.setText(ch_act_model.getName());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.On_View_Click(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{

        ImageView img ;
        TextView txt  ;
        LinearLayout linearLayout ;
        public VHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.act_img);
            txt=itemView.findViewById(R.id.act_name);
            linearLayout = itemView.findViewById(R.id.act_lin);
        }
    }

    public interface Click {
        public void On_View_Click(int position);
    }
}
