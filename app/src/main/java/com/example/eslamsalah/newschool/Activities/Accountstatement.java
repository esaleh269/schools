package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Accountstatement extends AppCompatActivity {
    Toolbar toolbar ;
    RequestQueue requestQueue;
    Locale locale ;
    Intent intent;
    String year;
    int studid;
    JSONObject jsonObject;
    TextView keenum, receiptnum, date, credit, depit, note, type;
    TextView keenum1, receiptnum1, date1, credit1, depit1, note1, type1;
    TextView madeen, credittotal, balance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountstatement);
        toolbar = findViewById(R.id.toolbar);
        keenum = findViewById(R.id.textView13);
        receiptnum = findViewById(R.id.textView14);
        date = findViewById(R.id.textView15);
        credit = findViewById(R.id.textView16);
        depit = findViewById(R.id.textView17);
        note = findViewById(R.id.textView18);
        type = findViewById(R.id.textView19);
        keenum1 = findViewById(R.id.textView23);
        receiptnum1 = findViewById(R.id.textView24);
        date1 = findViewById(R.id.textView25);
        credit1 = findViewById(R.id.textView26);
        depit1 = findViewById(R.id.textView27);
        note1 = findViewById(R.id.textView28);
        type1 = findViewById(R.id.textView29);
        madeen = findViewById(R.id.textView48);
        credittotal = findViewById(R.id.textView42);
        balance = findViewById(R.id.textView5);
        setSupportActionBar(toolbar);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadLanguage();
        Getyear();
        GetAccountstatement();
    }

    public void Getyear() {
        requestQueue = Volley.newRequestQueue(this);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Result/GetYear?id=15", null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                jsonObject = (JSONObject) response.get(i);
                                year = (jsonObject.getString("year"));
                                SharedPreferences pref = getSharedPreferences("info", MODE_PRIVATE);
                                SharedPreferences.Editor editor2 = pref.edit();
                                editor2.putString("year", year);
                                editor2.apply();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }


    public void GetAccountstatement() {
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        studid = preferences.getInt("studentid", 0);
        //  Toast.makeText(getBaseContext(),"i"+studid,Toast.LENGTH_LONG).show();
        year = preferences.getString("year", "bb");

        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Student/GetStudentQued?schoolId=" + Data.school_id + "&StudentUserId=" + studid + "&year=" + year, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            madeen.setText(response.getString("TotalQuedAmountD"));
                            credittotal.setText(response.getString("TotalQuedAmountC"));
                            balance.setText(response.getString("Total"));
                            JSONArray jsonArray = response.getJSONArray("QuedApi");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = (JSONObject) jsonArray.get(0);
                                keenum.setText(object.getString("QuedNum"));
                                receiptnum.setText(object.getString("QuedSanadNum"));
                                date.setText(object.getString("QuedPostDate"));
                                credit.setText(object.getString("QuedAmountC"));
                                depit.setText(object.getString("QuedAmountD"));
                                note.setText(object.getString("QuedDiscr"));
                                type.setText(object.getString("QuedSanadKind"));
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = (JSONObject) jsonArray.get(1);
                                keenum1.setText(object.getString("QuedNum"));
                                receiptnum1.setText(object.getString("QuedSanadNum"));
                                date1.setText(object.getString("QuedPostDate"));
                                credit1.setText(object.getString("QuedAmountC"));
                                depit1.setText(object.getString("QuedAmountD"));
                                note1.setText(object.getString("QuedDiscr"));
                                type1.setText(object.getString("QuedSanadKind"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
