package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.eslamsalah.newschool.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Map extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        String lon = intent.getStringExtra("long");
        String lan = intent.getStringExtra("lat");

        double longitude = Double.parseDouble(lon);
        double latitude = Double.parseDouble(lan);


        LatLng shebin = new LatLng(longitude, latitude);
        mMap.addMarker(new MarkerOptions().position(shebin).title("Pioneers Edustepup Demo"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(shebin,12));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
