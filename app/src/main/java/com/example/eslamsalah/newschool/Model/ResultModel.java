package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/18/2017.
 */

public class ResultModel {
    private String subject ;
    private String degree ;
    private String high ;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }
}
