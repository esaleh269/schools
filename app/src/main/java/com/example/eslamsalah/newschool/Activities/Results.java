package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Result_Recycler;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.ResultModel;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Results extends AppCompatActivity {

    public static String type;
    public static String photo;
    public static String Names[];
    public String Year;
    android.support.v7.widget.Toolbar toolbar ;
    RequestQueue requestQueue ;
    Spinner spinner ;
    ArrayAdapter arrayAdapter ;
    Intent intent ;
    RecyclerView recyclerView ;
    Result_Recycler result_recycler ;
    ArrayList<ResultModel> Res_List ;
    Locale locale ;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Names = new String[]{getResources().getString(R.string.fterm),getResources().getString(R.string.sterm),
                getResources().getString(R.string.srfterm),getResources().getString(R.string.srsterm),
        };
        intent = getIntent();
        progressDialog = new ProgressDialog(Results.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        toolbar = findViewById(R.id.resroolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.result));
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });
        Year = "";
        GetYear ();
    }
     public void GetYear (){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,

                "http://epi.pioneers-solutions.org/api/Result/GetYear?id=" + Data.school_id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i=0;i<response.length();i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Year = jsonObject.getString("year");
                                SetSpinner(Year);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
       }

    public void SetSpinner(final String yearr){


        spinner = findViewById(R.id.res_spinner);
        arrayAdapter = new ArrayAdapter<String>(Results.this,android.R.layout.simple_spinner_item,Names);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

             type =   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
                String type_val = "";

                if (type.equals(getResources().getString(R.string.fterm))){
                    type_val = "First";
                }else if (type.equals(getResources().getString(R.string.sterm))){
                    type_val = "Second";
                }else if (type.equals(getResources().getString(R.string.srsterm))){
                    type_val = "First2";
                }else if (type.equals(getResources().getString(R.string.srsterm))){
                    type_val = "Second2";
                }

             GetAbsencee(type_val,yearr);

             }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

             }
        });

     }

     public void GetAbsencee (String typee , String year){


         int id = intent.getIntExtra("id",0);
         requestQueue = Volley.newRequestQueue(this);

         JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                 "http://epi.pioneers-solutions.org/api/Result/GetStudentResult?id=" + Data.school_id + "&uid=" + id + "&year=" + year + "&type=" + typee, null,

                 new Response.Listener<JSONArray>() {
                     @Override
                     public void onResponse(JSONArray response) {
                         Res_List = new ArrayList<>(response.length()+1);
                         ResultModel resultModel1 = new ResultModel();
                         resultModel1.setSubject(getResources().getString(R.string.subjectt));
                         resultModel1.setHigh(getResources().getString(R.string.high));
                         resultModel1.setDegree(getResources().getString(R.string.degree));
                         Res_List.add(resultModel1);
                         for (int i=0 ; i<response.length();i++){
                             try {
                                 JSONObject jsonObject = (JSONObject) response.get(i);
                                 ResultModel resultModel = new ResultModel();
                                 resultModel.setSubject(jsonObject.getString("Subject"));
                                 resultModel.setHigh(jsonObject.getString("High"));
                                 resultModel.setDegree(jsonObject.getString("Degree"));

                                 Res_List.add(resultModel);

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }
                         }

                         recyclerView = findViewById(R.id.res_recycler);
                         result_recycler = new Result_Recycler(Res_List,Results.this);
                         LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Results.this,LinearLayoutManager.VERTICAL,false);
                         recyclerView.setLayoutManager(linearLayoutManager);
                         recyclerView.setItemAnimator(new DefaultItemAnimator());
                         recyclerView.setAdapter(result_recycler);
                         progressDialog.dismiss();

                      }
                 },
                 new Response.ErrorListener() {
                     @Override
                     public void onErrorResponse(VolleyError error) {
                         Toast.makeText(Results.this, ""+error.toString(), Toast.LENGTH_SHORT).show();
                     }
                 }
         );

          requestQueue.add(jsonArrayRequest);
      }
    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

}
