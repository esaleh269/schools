package com.example.eslamsalah.newschool.Model;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;


public class GcmTokenRefreshListner extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent intent = new Intent(this, GcmRegistrationService.class);
        startActivity(intent);
    }
}
