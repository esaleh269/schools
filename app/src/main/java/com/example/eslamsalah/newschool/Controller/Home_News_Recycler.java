package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.Home_News_Model;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home_News_Recycler extends RecyclerView.Adapter<Home_News_Recycler.VHolder> {

    private Context context ;
    private ArrayList<Home_News_Model> News_List ;

    public Home_News_Recycler(Context context1 , ArrayList<Home_News_Model> news_List1){

        context = context1 ;
        News_List = news_List1 ;

    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_news_view_shape,parent,false);

        return new Home_News_Recycler.VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {
        Home_News_Model parentNewsModel = News_List.get(position);
        holder.title.setText(parentNewsModel.getTitle());
        holder.desc.setText(parentNewsModel.getDesc());
        holder.date.setText(parentNewsModel.getDate());
        Picasso.with(context).load(parentNewsModel.getImg()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return News_List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{
        ImageView imageView ;
        TextView title , desc , date ;

        public VHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imagee);
            title = itemView.findViewById(R.id.title);
            desc = itemView.findViewById(R.id.desc);
            date = itemView.findViewById(R.id.date);
        }
    }
}
