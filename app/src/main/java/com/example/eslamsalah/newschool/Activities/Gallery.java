package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Gallery extends AppCompatActivity {

    Toolbar toolbar ;
    RequestQueue requestQueue ;
    ArrayList<String> Img_List ;
    Locale locale ;
    ProgressDialog progressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        progressDialog = new ProgressDialog(Gallery.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        toolbar = findViewById(R.id.gallerytoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.gallery));
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Get_Gallery ();
    }

    public void Get_Gallery (){
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/Gallary?id=" + Data.school_id, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Img_List = new ArrayList<>(response.length());
                        for (int i=0 ; i<response.length() ; i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                String url = jsonObject.getString("Photo");
                                String urlsub = url.substring(2,url.length());
                                String img = dom+urlsub ;
                                Img_List.add(img);
                                progressDialog.dismiss();

                             } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        ViewPager mViewPager = findViewById(R.id.viewPageAndroid);
                        ImageAdapter adapterView = new ImageAdapter(Img_List,Gallery.this);
                        mViewPager.setAdapter(adapterView);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

    public class  ImageAdapter extends PagerAdapter {
        Context mContext;
        ArrayList<String> List ;


       public ImageAdapter(ArrayList<String> arrayList,Context context) {
            this.List =arrayList ;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return List.size();
        }


        @Override
        public boolean isViewFromObject(View v, Object obj) {
            return v == obj;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int i) {
            ImageView mImageView = new ImageView(mContext);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(mContext).load(List.get(i)).into(mImageView);
            container.addView(mImageView, 0);
            return mImageView;
        }

         @Override
        public void destroyItem(ViewGroup container, int i, Object obj) {
             container.removeView((ImageView) obj);
        }
    }

}
