package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.Home_News_Model;
import com.example.eslamsalah.newschool.Model.Parent_News_Model;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Parent_News_Recycler extends RecyclerView.Adapter<Parent_News_Recycler.VHolder> {

    private Context context ;
    private ArrayList<Parent_News_Model> News_List ;

    public Parent_News_Recycler(Context context1 , ArrayList<Parent_News_Model> news_List1){

        context = context1 ;
        News_List = news_List1 ;

    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_news_view_shape,parent,false);

        return new Parent_News_Recycler.VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {
        Parent_News_Model parentNewsModel = News_List.get(position);
        holder.title.setText(parentNewsModel.getTitle());
        holder.desc.setText(parentNewsModel.getDesc());
        holder.date.setText(parentNewsModel.getDate());
        Picasso.with(context).load(parentNewsModel.getImg()).into(holder.imageView);
     }

    @Override
    public int getItemCount() {
        return News_List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{
        ImageView imageView ;
        TextView title , desc , date ;

        public VHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.pimagee);
            title = itemView.findViewById(R.id.ptitle);
            desc = itemView.findViewById(R.id.pdesc);
            date = itemView.findViewById(R.id.pdate);
        }
    }
}
