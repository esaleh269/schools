package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.LPModel;
import com.example.eslamsalah.newschool.R;

import java.util.ArrayList;



public class LpRecycler extends RecyclerView.Adapter<LpRecycler.Vholder> {

    ArrayList<LPModel> LPList ;
    Context context ;

    public LpRecycler (  ArrayList<LPModel> LPList, Context context ){

        this.LPList = LPList ;
        this.context = context ;
    }
    @Override
    public Vholder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lpviewshape,parent,false);
       return new LpRecycler.Vholder(view);
    }

    @Override
    public void onBindViewHolder(Vholder holder, int position) {

        LPModel lpModel = LPList.get(position);
        holder.reson.setText(lpModel.getReason());
        holder.date.setText(lpModel.getDate());


        if (position==0){
            holder.reson.setTextColor(Color.BLACK);
            holder.date.setTextColor(Color.BLACK);

            holder.reson.setTypeface(Typeface.DEFAULT_BOLD);
            holder.date.setTypeface(Typeface.DEFAULT_BOLD);

        }

    }

    @Override
    public int getItemCount() {
        return LPList.size();
    }

    public class Vholder extends RecyclerView.ViewHolder{

        TextView reson , date ;
        public Vholder(View itemView) {
            super(itemView);

            reson = itemView.findViewById(R.id.lpreason);
            date = itemView.findViewById(R.id.lpdate);
        }
    }
}
