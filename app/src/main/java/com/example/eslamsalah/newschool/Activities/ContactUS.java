package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class ContactUS extends AppCompatActivity {

    android.support.v7.widget.Toolbar toolbar ;
    RequestQueue requestQueue ;
    TextView Name , Governorate , Department , Mobile ;
    ImageView Facebook , Instagram , Twetter , Site ;
    String f_link , i_link , t_link , s_link , longitude , latitude;
    Locale locale ;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_contact_us);
        progressDialog = new ProgressDialog(ContactUS.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        toolbar=findViewById(R.id.cont_us_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.contact_us));
        toolbar.setTitleTextColor(Color.WHITE);

        Name = findViewById(R.id.cont_name);
        Governorate = findViewById(R.id.cont_gov);
        Department = findViewById(R.id.cont_dep);
        Mobile = findViewById(R.id.cont_mobile);
        Facebook = findViewById(R.id.facebook);
        Instagram = findViewById(R.id.instagram);
        Twetter = findViewById(R.id.twetter);
        Site = findViewById(R.id.site);

         Set_Data ();
     }

    public void Set_Data (){

         requestQueue = Volley.newRequestQueue(this);
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/Contactus?id=" + Data.school_id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Governorate.setText(response.getString("Governrateen"));
                            Department.setText(response.getString("Edaraen"));
                            Mobile.setText(response.getString("Phone"));
                            f_link = response.getString("Facebook");
                            i_link  =response.getString("Instagram");
                            t_link = response.getString("Twitter");
                            s_link = response.getString("Domain");
                            longitude = response.getString("longitude");
                            latitude= response.getString("latitude");
                            progressDialog.dismiss();

                            set_Action(f_link,i_link,t_link,s_link);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }

         );
        requestQueue.add(jsonObjectRequest);
        findViewById(R.id.mapb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactUS.this,Map.class);
                intent.putExtra("long",longitude);
                intent.putExtra("lat",latitude);
                startActivity(intent);
            }
        });



    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }


    public void set_Action (final String fac , final String inst , final String twet , final String site){

        Facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(fac));
                startActivity(intent);
            }
        });

        Instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(inst));
                startActivity(intent);
            }
        });
        Twetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twet));
                startActivity(intent);
            }
        });
        Site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(site));
                startActivity(intent);
            }
        });
    }
}
