package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Voting extends AppCompatActivity {

    public static int vote_id = 0;
    public static String vote_name = "";
    Toolbar toolbar ;
    Locale locale ;
    RequestQueue requestQueue;
    RadioGroup Votes_Radio_Group;
    TextView Vote_Quest;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadLanguage();
        GetVoteOptions();
    }

    public void GetVoteOptions() {
        Vote_Quest = findViewById(R.id.vote_text);
        Votes_Radio_Group = findViewById(R.id.votes_radio_group);

        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Vote/GetLastVote?schoolId=" + Data.school_id, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Vote_Quest.setText(response.getString("VoiceTitle"));
                            vote_id = response.getInt("VoiceId");

                            JSONArray jsonArray = response.getJSONArray("VotingOptions");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = (JSONObject) jsonArray.get(i);
                                final RadioButton radioButton = new RadioButton(Voting.this);
                                radioButton.setText(object.getString("OptionName"));
                                Votes_Radio_Group.addView(radioButton);
                                radioButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        vote_name = (String) radioButton.getText();
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
        findViewById(R.id.send_vote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoteReply(vote_id, vote_name);
            }
        });

    }


    public void VoteReply(int vote_iid, String vote_nname) {
        requestQueue = Volley.newRequestQueue(this);
        intent = getIntent();
        int sid = intent.getIntExtra("studentid", 0);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("SchoolId", Data.school_id);
            jsonObject.put("StudentUserId", sid);
            jsonObject.put("VoiteId", vote_iid);
            jsonObject.put("OptionName", vote_nname);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                "http://epi.pioneers-solutions.org/api/Vote/VoiteReplay", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String tt = response.getString("Response");
                            if (tt.equals("Done")) {
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(R.id.vot_lin), "Thanks", Snackbar.LENGTH_LONG);

                                View sbView = snackbar.getView();
                                sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                                textView.setTextColor(Color.WHITE);
                                snackbar.show();
                            } else {
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Voting.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
