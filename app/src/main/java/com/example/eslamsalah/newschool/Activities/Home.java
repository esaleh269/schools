package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Home_News_Recycler;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.Home_News_Model;
import com.example.eslamsalah.newschool.R;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.rom4ek.arcnavigationview.ArcNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;


public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static String Domain = "http://elminiakawmia.com/";
    Toolbar toolbar ;
    ArcNavigationView navigationView ;
    DrawerLayout drawerLayout;
    CoordinatorLayout coordinatorLayout ;
    RequestQueue requestQueue ;
    ArrayList <Home_News_Model> News ;
    RecyclerView recyclerView ;
    Home_News_Recycler home_news_recycler;
    Locale locale ;
    ProgressDialog progressDialog ;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_home);

         progressDialog = new ProgressDialog(Home.this,
                 R.style.Theme_AppCompat_DayNight_Dialog);
         progressDialog.setIndeterminate(false);
         progressDialog.setMessage("Loading...");
         progressDialog.show();
        coordinatorLayout = findViewById(R.id.home_coordinator);
        toolbar = findViewById(R.id.hometoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);

        set_Bottom_Nav(savedInstanceState);
        GetSchools();
        Get_Neww();



     }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    public void Get_Neww (){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/GetNews?id=" + Data.school_id, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        News = new ArrayList<>(response.length());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Home_News_Model parentNewsModel = new Home_News_Model();
                                parentNewsModel.setTitle(jsonObject.getString("Name"));
                                parentNewsModel.setDate(jsonObject.getString("NewsDate"));
                              //  parentNewsModel.setDesc(jsonObject.getString("Description"));
                                parentNewsModel.setDesc(Html.fromHtml(jsonObject.getString("Description")).toString());
                                String x = jsonObject.getString("Photo");
                                String xx = x.substring(2, x.length());
                                String xxx = Domain + xx;
                                parentNewsModel.setImg(xxx);
                                News.add(parentNewsModel);
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        recyclerView = findViewById(R.id.home_recyclerView);
                        home_news_recycler = new Home_News_Recycler(Home.this, News);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Home.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(home_news_recycler);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
     }

    public void  GetSchools(){

        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/login/getschool?id=" + Data.school_id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Domain = response.getString("DomainName");
                            SharedPreferences pref = getSharedPreferences("exinfo",MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = pref.edit();
                            editor1.putString("dom",Domain);
                            editor1.apply();
                         } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(jsonObjectRequest);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hommenu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home_login:
                Intent intent = new Intent(Home.this,Login.class);
                startActivity(intent);
                break;
            case R.id.home_settings:
                Intent intent2 = new Intent(Home.this,Settings.class);
                startActivity(intent2);
                break;
        }
        return true;
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }


    public void set_Bottom_Nav (Bundle save){


        SpaceNavigationView spaceNavigationView = findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(save);
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.contact_us), R.drawable.contact24));
        spaceNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.profile), R.drawable.profile24));
        if (getLangCode().equals("ar")){
            spaceNavigationView.showTextOnly();
        }

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

                 Intent intent = new Intent(Home.this,Login.class);
                 startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {


                if (itemIndex==1){
                    SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
                    String userType = preferences.getString("UserType", null);
                    int userId = preferences.getInt("UserID", 0);

                    if (userId==0){

                        Toast.makeText(Home.this, "You Must Login", Toast.LENGTH_SHORT).show();

                    }else {
                        if (userType.equals("Parent")){
                            Intent intent = new Intent(Home.this,Parent_Profile.class);
                            intent.putExtra("uid",userId);

                            startActivity(intent);
                        }else{

                            Intent intent = new Intent(Home.this, Studentprofile.class);
                            intent.putExtra("sid",userId);
                            intent.putExtra("state","home");
                            startActivity(intent);
                        }
                    }
                }else {
                    Intent intent = new Intent(Home.this,ContactUS.class);
                    startActivity(intent);
                }


             }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {


                if (itemIndex==1){
                    SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
                    String userType = preferences.getString("UserType", null);
                    int userId = preferences.getInt("UserID", 0);



                    if (userId==0){

                        Toast.makeText(Home.this, "You Must Login", Toast.LENGTH_SHORT).show();

                    }else {
                        if (userType.equals("Parent")){
                            Intent intent = new Intent(Home.this,Parent_Profile.class);
                            intent.putExtra("uid",userId);
                            startActivity(intent);
                        }else{

                            Intent intent = new Intent(Home.this,Student_Profile.class);
                            intent.putExtra("sid",userId);
                            intent.putExtra("state","home");
                            startActivity(intent);
                        }
                    }
                }else {
                    Intent intent = new Intent(Home.this,ContactUS.class);
                    startActivity(intent);
                }
             }
        });
        spaceNavigationView.setCentreButtonColor(getResources().getColor(R.color.c1));
        spaceNavigationView.setCentreButtonIcon(R.drawable.acclogin24);
     }


}
