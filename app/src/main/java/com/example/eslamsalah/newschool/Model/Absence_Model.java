package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/19/2017.
 */

public class Absence_Model {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
