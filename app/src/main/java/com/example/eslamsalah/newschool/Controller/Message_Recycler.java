package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Message_Model;
import com.example.eslamsalah.newschool.R;

import org.json.JSONObject;

import java.util.ArrayList;


public class Message_Recycler extends RecyclerView.Adapter<Message_Recycler.VHolder> {

    ArrayList<Message_Model> Msg_List ;
    Context context ;
    RequestQueue requestQueue ;
    MSG_CLick msg_cLick ;

    public Message_Recycler (  ArrayList<Message_Model> Msg_Listt,Context contextt ){

        Msg_List = Msg_Listt ;
        context = contextt ;
    }
    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_view_shape,parent,false);
        return new Message_Recycler.VHolder(view);
    }

    public void SetListner (MSG_CLick msg_cLick1){
        this.msg_cLick=msg_cLick1;
    }

    @Override
    public void onBindViewHolder(final VHolder holder, final int position) {

        Message_Model messageModel = Msg_List.get(position);
        holder.title.setText(messageModel.getTitle());
        holder.desc.setText(messageModel.getDesc());
        holder.date.setText(messageModel.getDate());

        if (Msg_List.get(position).getState().equals("1")){

            holder.title.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.desc.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.date.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sendReq(Msg_List.get(position).getId());
               msg_cLick.OnMsgClick(position);
             }
        });

        }

         public void sendReq(String id){
         requestQueue = Volley.newRequestQueue(context);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    "http://epi.pioneers-solutions.org/api/ParentInbox/readmessage?id=11&messageid="+id, null,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            );
            requestQueue.add(jsonObjectRequest);

          }


    @Override
    public int getItemCount() {
        return Msg_List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        TextView title , date , desc ;
        LinearLayout linearLayout ;

        public VHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.msg_title);
            desc = itemView.findViewById(R.id.msg_desc);
            date = itemView.findViewById(R.id.msg_date);
            linearLayout = itemView.findViewById(R.id.readmsg_lin);

          }
    }

    public interface MSG_CLick {
             public void OnMsgClick(int position);
    }
}
