package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONObject;

import java.util.Locale;

public class ReplyMessage extends AppCompatActivity {

    Toolbar toolbar ;
    TextView From , Subject , OriginalMSg ;
    EditText Type_Reply ;
    Button SendReply ;
    Intent intent ;
    RequestQueue requestQueue ;
    LinearLayout linearLayout ;
    Locale locale ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_reply_message);

        linearLayout=findViewById(R.id.reply_lin);
        toolbar=findViewById(R.id.replt_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.reply));
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SetData();


    }

    public void SetData (){
        intent=getIntent();
        From = findViewById(R.id.reply_from);
        Subject = findViewById(R.id.reply_subject);
        OriginalMSg= findViewById(R.id.original_msg);
        Type_Reply = findViewById(R.id.type_reply);
        SendReply = findViewById(R.id.send_reply);

        From.setText(intent.getStringExtra("sender"));
        Subject.setText(intent.getStringExtra("mytitle"));
        OriginalMSg.setText(intent.getStringExtra("original"));



        SendReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SendReplyy(Subject.getText().toString(),OriginalMSg.getText().toString()+",,"+Type_Reply.getText().toString());
                Type_Reply.setText("");

            }
        });


    }

    public void SendReplyy(String subject , String description){

        requestQueue= Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        int uid = preferences.getInt("UserID", 0);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/ParentInbox/Getpostmessage?id=" + Data.school_id + "&uid=" + uid + "&message=" + description + "&subject=" + subject, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, "Message Sent", Snackbar.LENGTH_LONG);
                        View sbView = snackbar.getView();
                        sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ReplyMessage.this, ""+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                "http://epi.pioneers-solutions.org/api/ParentInbox/Getpostmessage?id=11&uid="+uid+"&message="+description+"&subject="+subject,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                            if (!response.isEmpty()){
//
//
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(ReplyMessage.this, ""+ error.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//        );
//        requestQueue.add(stringRequest);
     }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

}
