package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.ResultModel;
import com.example.eslamsalah.newschool.R;

import java.util.ArrayList;

/**
 * Created by eslam.salah on 12/18/2017.
 */

public class Result_Recycler extends RecyclerView.Adapter<Result_Recycler.VHolder> {

    ArrayList<ResultModel> Res_List ;
    Context context ;

    public Result_Recycler ( ArrayList<ResultModel> Res_Listt,Context context1){
        this.Res_List = Res_Listt ;
        this.context = context1 ;

    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.results_viewshape,parent,false);

        return new Result_Recycler.VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {

        ResultModel resultModel = Res_List.get(position);
        holder.subject.setText(String.valueOf(resultModel.getSubject()));
        holder.high.setText(String.valueOf(resultModel.getHigh()));
        holder.degree.setText(String.valueOf(resultModel.getDegree()));

        if (position==0){
            holder.subject.setTextColor(Color.BLACK);
            holder.high.setTextColor(Color.BLACK);
            holder.degree.setTextColor(Color.BLACK);

            holder.subject.setTypeface(Typeface.DEFAULT_BOLD);
            holder.high.setTypeface(Typeface.DEFAULT_BOLD);
            holder.degree.setTypeface(Typeface.DEFAULT_BOLD);



        }


    }

    @Override
    public int getItemCount() {
        return Res_List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{

        TextView subject ,high , degree ;

        public VHolder(View itemView) {
            super(itemView);

            subject = itemView.findViewById(R.id.subject);
            high = itemView.findViewById(R.id.high);
            degree = itemView.findViewById(R.id.degree);
        }
    }

}
