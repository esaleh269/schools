package com.example.eslamsalah.newschool.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Children_Recycler;
import com.example.eslamsalah.newschool.Controller.Parent_News_Recycler;
import com.example.eslamsalah.newschool.Model.Children_Model;
import com.example.eslamsalah.newschool.Model.Parent_News_Model;
import com.example.eslamsalah.newschool.R;
import com.rom4ek.arcnavigationview.ArcNavigationView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Parent_Profile extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,Children_Recycler.Child_click {

     android.support.v7.widget.Toolbar toolbar ;
     DrawerLayout drawerLayout ;
     ArcNavigationView arcNavigationView ;
     RequestQueue requestQueue ;
     ArrayList<Parent_News_Model> News_List ;
     RecyclerView recyclerView ;
     Parent_News_Recycler parent_news_recycler ;
     Intent intent ;
     ImageView userimg ;
     TextView username ;
     String img,name , photo ;
     Dialog dialog ;
     ArrayList<Children_Model> ChildList ;
     RecyclerView childrec ;
     Children_Recycler children_recycler ;
     FloatingActionButton fab ;
     Locale locale ;
     ProgressDialog progressDialog ;
     long [] Data  ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_parent__profile);
        progressDialog = new ProgressDialog(Parent_Profile.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        drawerLayout = findViewById(R.id.parent_drawer);
        toolbar = findViewById(R.id.parent_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.parent));
        toolbar.setTitleTextColor(Color.WHITE);
        arcNavigationView = findViewById(R.id.parent_navigation);
        arcNavigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        toggle.syncState();
        drawerLayout.addDrawerListener(toggle);
        Get_News();
        GetParent ();
        GetChildren ();
        findViewById(R.id.showchildren).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        setCount();
        getdataaa();
        Send_Token();
    }

    public void Get_News (){
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/GetNews?id=" + com.example.eslamsalah.newschool.Model.Data.school_id, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        News_List = new ArrayList<>(response.length());
                        for (int i=0 ; i<response.length();i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Parent_News_Model parentNewsModel =new Parent_News_Model();
                                parentNewsModel.setTitle(jsonObject.getString("Name"));
                                parentNewsModel.setDate(jsonObject.getString("NewsDate"));
                              //  parentNewsModel.setDesc(jsonObject.getString("Description"));
                                parentNewsModel.setDesc(Html.fromHtml(jsonObject.getString("Description")).toString());
                                String x = jsonObject.getString("Photo");
                                String xx= x.substring(2,x.length());
                                String xxx = dom+xx ;
                                parentNewsModel.setImg(xxx);
                                News_List.add(parentNewsModel);
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            recyclerView = findViewById(R.id.parent_recycler);
                            parent_news_recycler = new Parent_News_Recycler(Parent_Profile.this,News_List);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Parent_Profile.this,LinearLayoutManager.VERTICAL,false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(parent_news_recycler);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Parent_Profile.this, "get news :: "+error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }

    public void GetParent (){
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);
        SharedPreferences preferences2 = getSharedPreferences("info", MODE_PRIVATE);
        int userId = preferences2.getInt("UserID", 0);
        requestQueue = Volley.newRequestQueue(this);
       // final int uid = intent.getIntExtra("uid",0);
        userimg = toolbar.findViewById(R.id.parent_img);
        username = toolbar.findViewById(R.id.parent_name);



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/login/getuser?id=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&uid=" + userId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            name = response.getString("Name");
                            img = response.getString("Photo");
                            String xx = img.substring(2,img.length());
                            photo= dom+xx ;
                         } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        username.setText(name);
                        Picasso.with(Parent_Profile.this).load(photo).into(userimg);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                       Toast.makeText(Parent_Profile.this, "get parent ::"+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);

     }

    public void GetChildren (){

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.child_dialog);
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        final int userId = preferences.getInt("UserID", 0);
     //   final int uid = intent.getIntExtra("uid",0);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/ParentInbox/Getchilderen?id=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&uid=" + userId, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ChildList = new ArrayList<>(response.length());
                        for (int i=0 ; i<response.length() ; i++){

                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Children_Model childrenModel = new Children_Model();
                                childrenModel.setChildName(jsonObject.getString("Stuname"));
                                childrenModel.setChildId(jsonObject.getInt("UserID"));
                                ChildList.add(childrenModel);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                          }

                        childrec = dialog.findViewById(R.id.childrec);
                        children_recycler = new Children_Recycler(ChildList,Parent_Profile.this);
                        children_recycler.SetListner(Parent_Profile.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Parent_Profile.this,LinearLayoutManager.VERTICAL,false);
                        childrec.setLayoutManager(linearLayoutManager);
                        childrec.setItemAnimator(new DefaultItemAnimator());
                        childrec.setAdapter(children_recycler);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Parent_Profile.this, "get chioldren ::"+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }

    public void setCount(){

        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        final int userId = preferences.getInt("UserID", 0);
      //  int uid = intent.getIntExtra("uid",0);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/ParentInbox/getinpoxnumber?id=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&uid=" + userId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        Menu menuNav = arcNavigationView.getMenu();
                        MenuItem element = menuNav.findItem(R.id.msg_icon);
                        String before = element.getTitle().toString();


                       // String counter = Integer.toString(25);
                        String s = before + "   "+ response +" ";
                        SpannableString sColored = new SpannableString( s );

                        sColored.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.gray)), s.length()-(response.length()+2), s.length(), 0);
                        sColored.setSpan(new ForegroundColorSpan( Color.RED ), s.length()-(response.length()+2), s.length(), 0);


                        element.setTitle(sColored);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Parent_Profile.this, "get count ::"+error.toString(), Toast.LENGTH_SHORT).show();


                    }
                }
        );
        requestQueue.add(stringRequest);

    }
     @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

//           int uid = intent.getIntExtra("uid",0);

           switch (item.getItemId()){
            case R.id.child_icon:
                dialog.show();
                drawerLayout.closeDrawers();
                break;
            case R.id.msg_icon:
                Intent intentm = new Intent(Parent_Profile.this,Messages.class);
             //   intentm.putExtra("pid",uid);
                intentm.putExtra("pname",name);
                startActivity(intentm);
                drawerLayout.closeDrawers();
                break;
            case R.id.calendar_icon:
                Intent intent = new Intent(Parent_Profile.this,Calendarr.class);
                Bundle bundle = new Bundle();
                bundle.putLongArray("data", Data);
                intent.putExtras(bundle);
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;
            case R.id.gallery_icon:
                Intent intent1 = new Intent(Parent_Profile.this,Gallery.class);
                startActivity(intent1);
                drawerLayout.closeDrawers();
                break;

            case R.id.contactus_icon:
                Intent intent2 = new Intent(Parent_Profile.this,ContactUS.class);
                startActivity(intent2);
                drawerLayout.closeDrawers();
                break;

            case R.id.logout_icon:
                SharedPreferences pref = getSharedPreferences("info",MODE_PRIVATE);
                SharedPreferences.Editor editor1 = pref.edit() ;
                editor1.putInt("UserID",0);
                editor1.apply();
                Intent intent3 = new Intent(Parent_Profile.this,Home.class);
                startActivity(intent3);
                drawerLayout.closeDrawers();
                break;

               case R.id.setting_icon:
                   Intent i = new Intent(Parent_Profile.this,Settings.class);
                   startActivity(i);
                   drawerLayout.closeDrawers();
                   break;

        }

        return false;
    }

    public void getdataaa(){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/Events?id=" + com.example.eslamsalah.newschool.Model.Data.school_id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Data =  new long[response.length()];

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);

                                String x = jsonObject.getString("Totalmilliseconds");
                                long a = Long.parseLong(x) ;
                                 Data[i]=a ;



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Parent_Profile.this, "get Events ::" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);



    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

    @Override
    public void ChooseChild(int position) {

         Intent intent = new Intent(Parent_Profile.this,Studentprofile.class);
         intent.putExtra("sid",ChildList.get(position).getChildId());
         intent.putExtra("state","parent");

        startActivity(intent);
        Intent intent1 = new Intent(Parent_Profile.this,Complaint.class);
        intent1.putExtra("sid",ChildList.get(position).getChildId());
        Intent intent2 = new Intent(Parent_Profile.this, Studentprofile.class);
        intent2.putExtra("stid", ChildList.get(position).getChildId());
        dialog.dismiss();

    }

    public void Send_Token(){
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferencesu = getSharedPreferences("info", MODE_PRIVATE);
        int userId = preferencesu.getInt("UserID", 0);

        SharedPreferences preferences = getSharedPreferences("settoken",MODE_PRIVATE);
        String   token =  preferences.getString("tokval",null);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("deviceId",token);
            jsonObject.put("parentId",String.valueOf(userId));
            jsonObject.put("SchoolId", com.example.eslamsalah.newschool.Model.Data.school_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "http://epi.pioneers-solutions.org/api/Notification/GetOldNotification",jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            String x =response.getString("statues");
                         //   Toast.makeText(Parent_Profile.this, ""+x, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Parent_Profile.this, ""+e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Parent_Profile.this, ""+error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );

        requestQueue.add(jsonObjectRequest);
    }


}
