package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Complaint extends AppCompatActivity {
    /////////////uploadimage
    private static int RESULT_LOAD_IMG = 1;
    RadioGroup language ;
    RadioButton complaint , suggestion ;
    LinearLayout linearLayout;
    TextView textView;
    EditText message;
    Toolbar toolbar ;
    Locale locale ;
    Intent intent ;
    RequestQueue requestQueue ;
    Button send;
    String imgDecodableString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);
        intent = getIntent();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        linearLayout = findViewById(R.id.setting);
        linearLayout.setVisibility(View.INVISIBLE);
        textView = findViewById(R.id.textView2);
        message = findViewById(R.id.edit1);
        send = findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.getText().toString().isEmpty()){
                    Toast.makeText(Complaint.this, "You Must Full contents", Toast.LENGTH_SHORT).show();
                }else {
                    Send_Data1 (textView.getText().toString(),message.getText().toString());

                }

            }
        });
        set_settings();
        loadLanguage();

    }
    public void set_settings (){
        language = findViewById(R.id.radiogroup);
        complaint = findViewById(R.id.compl);
        suggestion = findViewById(R.id.sugg);
        language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {

                    case R.id.compl:
                       complaint.setChecked(true);
                        suggestion.setChecked(false);
                        linearLayout.setVisibility(View.VISIBLE);
                        textView.setText(getString(R.string.complaint1));
                        break;
                    case R.id.sugg:
                        suggestion.setChecked(true);
                        complaint.setChecked(false);
                        linearLayout.setVisibility(View.VISIBLE);
                        textView.setText(getString(R.string.suggestion));
                        break;

                }
            }
        });

    }

    public void Send_Data1 (String sub , String mes){
        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        int userId = preferences.getInt("UserID", 0);
        final int uid = intent.getIntExtra("studentid",0);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("SchoolId", Data.school_id);
            jsonObject.put("parentUserId", userId);
            jsonObject.put("StudentUserId", uid);
            jsonObject.put("State", sub);
            jsonObject.put("Message", mes);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                "http://epi.pioneers-solutions.org/api/ComplaintsAndSuggestions/add", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String tt = response.getString("Response");
                            if (tt.equals("Done")) {
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(R.id.complaints), "Thanks", Snackbar.LENGTH_LONG);

                                View sbView = snackbar.getView();
                                sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                                textView.setTextColor(Color.WHITE);
                                snackbar.show();
                            } else {
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Complaint.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
    //////////uploadimage//////////////////////////////////////////////////
    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK

                    && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                // Get the cursor

                Cursor cursor = getContentResolver().query(selectedImage,

                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                ImageView imgView = findViewById(R.id.image);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));
            } else {

                Toast.makeText(this, "You haven't picked Image",

                        Toast.LENGTH_LONG).show();

            }

        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();

        }

    }
}
