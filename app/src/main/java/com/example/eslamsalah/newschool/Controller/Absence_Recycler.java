package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.Absence_Model;
import com.example.eslamsalah.newschool.R;

import java.util.ArrayList;

/**
 * Created by eslam.salah on 12/19/2017.
 */

public class Absence_Recycler extends RecyclerView.Adapter<Absence_Recycler.VHolder> {

    ArrayList<Absence_Model> Abs_List ;
    Context context ;

    public Absence_Recycler(ArrayList<Absence_Model> Abs_Listt ,Context contextt){
     Abs_List=Abs_Listt ;
     context=contextt ;
    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.abc_view_shape,parent,false);

        return new Absence_Recycler.VHolder(view);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {

        Absence_Model absence_model = Abs_List.get(position);
        holder.Absence_date.setText(absence_model.getDate());
     }

    @Override
    public int getItemCount() {
        return Abs_List.size();
    }

    public class VHolder extends RecyclerView.ViewHolder{

        TextView Absence_date ;
        public VHolder(View itemView) {
            super(itemView);
            Absence_date = itemView.findViewById(R.id.absence_date);
        }
    }
}
