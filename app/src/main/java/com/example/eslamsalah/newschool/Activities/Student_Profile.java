package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Ch_Activities_Recycler;
import com.example.eslamsalah.newschool.Model.Ch_Act_Model;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Student_Profile extends AppCompatActivity implements Ch_Activities_Recycler.Click {

    public static String Names[];
    public static int Pholtos[] = {R.drawable.hw, R.drawable.result, R.drawable.attendance,
            R.drawable.leaveperm, R.drawable.cal, R.drawable.gallery, R.drawable.lessontable,
            R.drawable.profile, R.drawable.logout
    };
    Intent intent ;
    RequestQueue requestQueue;
    ImageView userimg ;
    TextView username ;
    String img,name , photo ;
    ArrayList<Ch_Act_Model> Act_List ;
    RecyclerView chrecycler ;
    Ch_Activities_Recycler activities_recycler ;

//    public static String Names [] = {"Home Work","Result","Absence",
//    "Leave Permission ","Calendar","Gallery","Lesson Table",
//            "Edit Profile","Logout"
//    };
long[] Data;
    Locale locale;
      android.support.v7.widget.Toolbar toolbar ;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         loadLanguage();
         setContentView(R.layout.activity_student__profile);
        intent = getIntent();
        toolbar=findViewById(R.id.student_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.student));
        toolbar.setTitleTextColor(Color.WHITE);
         Names = new String[]{getResources().getString(R.string.homework),getResources().getString(R.string.result),getResources().getString(R.string.absence),
                getResources().getString(R.string.leavepermission),getResources().getString(R.string.calendar),getResources().getString(R.string.gallery),
                getResources().getString(R.string.lessontable),getResources().getString(R.string.editprofile),getResources().getString(R.string.logout),
        };

         GetStudent ();
         Set_Activities ();
         getdataa();
    }
    public void GetStudent (){

        final int uid = intent.getIntExtra("sid",0);
        String state = intent.getStringExtra("state");

        requestQueue = Volley.newRequestQueue(this);
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);
        userimg = toolbar.findViewById(R.id.student_img);
        username = toolbar.findViewById(R.id.student_name);

         JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                 "http://epi.pioneers-solutions.org/api/login/getuser?id=" + com.example.eslamsalah.newschool.Model.Data.school_id + "&uid=" + uid, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            name = response.getString("Name");
                            img = response.getString("Photo");
                            String xx = img.substring(2,img.length());
                            Toast.makeText(getBaseContext(),"id"+img,Toast.LENGTH_LONG).show();

                            photo= dom+xx ;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        username.setText(name);
if (img=="null"){
    userimg.setImageResource(R.drawable.icons8_person);
                       }
                        else {
    Picasso.with(Student_Profile.this).load(photo).into(userimg);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);

    }

    public void Set_Activities (){

        Act_List = new ArrayList<>(9);

        for (int i=0 ; i<9;i++){
            Ch_Act_Model ch_act_model = new Ch_Act_Model();
            ch_act_model.setName(Names[i]);
            ch_act_model.setImg(Pholtos[i]);

            Act_List.add(ch_act_model);
        }

        chrecycler = findViewById(R.id.student_rec);
        activities_recycler = new Ch_Activities_Recycler(Act_List,Student_Profile.this);
        activities_recycler.SetListner(Student_Profile.this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(Student_Profile.this,3);
        chrecycler.setLayoutManager(gridLayoutManager);
        chrecycler.setItemAnimator(new DefaultItemAnimator());
        chrecycler.setAdapter(activities_recycler);

      }

    public void getdataa(){
        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/Events?id=" + com.example.eslamsalah.newschool.Model.Data.school_id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Data =  new long[response.length()];

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);

                                String x = jsonObject.getString("Totalmilliseconds");
                                long a = Long.parseLong(x) ;
                                Data[i]=a ;



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Student_Profile.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);



    }

    @Override
    public void On_View_Click(int position) {


        final int id = intent.getIntExtra("sid",0);
       switch (position){
           case 0 :
               Intent i1 = new Intent(Student_Profile.this,Home_Work.class);
               i1.putExtra("id",id);
               startActivity(i1);
               break;
           case 1 :
               Intent i2 = new Intent(Student_Profile.this,Results.class);
               i2.putExtra("id",id);
               startActivity(i2);
               break;
           case 2 :
               Intent i3 = new Intent(Student_Profile.this,Absence.class);
               i3.putExtra("id",id);
               startActivity(i3);
               break;
           case 3 :
               Intent i4 = new Intent(Student_Profile.this,Leave_Permission_Report.class);
               i4.putExtra("id",id);
               startActivity(i4);
               break;
           case 4 :
               Intent i5 = new Intent(Student_Profile.this,Calendarr.class);
               i5.putExtra("id",id);
               Bundle bundle = new Bundle();
               bundle.putLongArray("data", Data);
               i5.putExtras(bundle);
               startActivity(i5);
               break;
           case 5 :
               Intent i6 = new Intent(Student_Profile.this,Gallery.class);
               i6.putExtra("id",id);
               startActivity(i6);
               break;
           case 6 :
               Intent i7 = new Intent(Student_Profile.this,Lesson_Table.class);
               i7.putExtra("id",id);
               startActivity(i7);
               break;
           case 7 :
               Intent i8 = new Intent(Student_Profile.this,Edit_Profile.class);
               i8.putExtra("id",id);
               startActivity(i8);
               break;
           case 8 :
               SharedPreferences pref = getSharedPreferences("info",MODE_PRIVATE);
               SharedPreferences.Editor editor1 = pref.edit() ;
               editor1.putInt("UserID",0);
               editor1.apply();
               Intent i9 = new Intent(Student_Profile.this,Home.class);
               startActivity(i9);
               break;
       }
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }
    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
