package com.example.eslamsalah.newschool.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Message_Recycler;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.Message_Model;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Studentmessages extends AppCompatActivity implements Message_Recycler.MSG_CLick {
    Toolbar toolbar ;
    Locale locale ;
    Intent i;
    RequestQueue requestQueue;
    RecyclerView recyclerView;
    Message_Recycler message_recycler;
    ArrayList<Message_Model> Msg_List;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_studentmessages);
        toolbar = findViewById(R.id.msg_toolbar);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Studentmessages.this,Studentprofile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        Get_Messages();

    }


    public void Get_Messages() {
        i = getIntent();
        int uid = i.getIntExtra("studentid", 0);
        requestQueue = Volley.newRequestQueue(this);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Message/GetAllMessageBy?SchoolId=" + Data.school_id + "&StudentUserId=" + uid, null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Msg_List = new ArrayList<>(response.length());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Message_Model messageModel = new Message_Model();
                                messageModel.setTitle(jsonObject.getString("Subject"));
                                messageModel.setDesc(jsonObject.getString("Message1"));
                                messageModel.setDate(jsonObject.getString("Date"));
                                messageModel.setState(jsonObject.getString("Read"));
                                messageModel.setId(jsonObject.getString("Id"));
                                Msg_List.add(messageModel);
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        recyclerView = findViewById(R.id.msg_rec);
                        message_recycler = new Message_Recycler(Msg_List, Studentmessages.this);
                        message_recycler.SetListner(Studentmessages.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Studentmessages.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(message_recycler);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }


    public void SendMessage(View view) {

        final EditText sub , desc ;
        Button send ;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.send_msg_dialg);
        sub = dialog.findViewById(R.id.msg_sub);
        desc = dialog.findViewById(R.id.msg_descc);
        send = dialog.findViewById(R.id.submit_send);
        dialog.show();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sub.getText().toString().isEmpty()|desc.getText().toString().isEmpty()){
                    Toast.makeText(Studentmessages.this, "You Must Full All Feeds", Toast.LENGTH_SHORT).show();
                }else {
                    // Send_Data (sub.getText().toString(),desc.getText().toString());
                    dialog.dismiss();
                }

            }
        });
    }


    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

    @Override
    public void OnMsgClick(int position) {

    }
}
//studentid