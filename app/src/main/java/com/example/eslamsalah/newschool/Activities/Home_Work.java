package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.HomeWork_Recycler;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.HomeWork_Model;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Home_Work extends AppCompatActivity {

    android.support.v7.widget.Toolbar toolbar ;
    RequestQueue requestQueue ;
    Intent intent ;
    ArrayList<HomeWork_Model> HW_LIST ;
    RecyclerView recyclerView ;
    HomeWork_Recycler homeWorkRecycler ;
    Locale locale ;
    ProgressDialog progressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_home__work);

        progressDialog = new ProgressDialog(Home_Work.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        intent=getIntent();
        toolbar = findViewById(R.id.homeworktoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home Work");
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")) {
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else{
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        GethomeWrok();
    }
     public void GethomeWrok (){
        requestQueue = Volley.newRequestQueue(this);
        int id = intent.getIntExtra("id",0);
        SharedPreferences pref = getSharedPreferences("child",MODE_PRIVATE);
        SharedPreferences.Editor editor1 = pref.edit();
        editor1.putInt("chid",id);
        editor1.apply();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/homework/GetStudentHomeWork?id=" + Data.school_id + "&uid=" + id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        HW_LIST=new ArrayList<>(response.length());
                        for (int i=0 ; i<response.length() ; i++){
                            HomeWork_Model homeWorkModel=new HomeWork_Model();
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                homeWorkModel.setQtitle(jsonObject.getString("Title"));
                                homeWorkModel.setQdesc(jsonObject.getString("HomeworkQu"));
                                homeWorkModel.setQid(jsonObject.getString("Id"));
                                HW_LIST.add(homeWorkModel);
                                progressDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        recyclerView=findViewById(R.id.hw_rec);
                        homeWorkRecycler=new HomeWork_Recycler(HW_LIST,Home_Work.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(Home_Work.this,LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(homeWorkRecycler);
                     }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Home_Work.this, ""+error.toString(), Toast.LENGTH_LONG).show();


                     }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }



}
