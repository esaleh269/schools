package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.eslamsalah.newschool.R;

import java.util.Locale;

public class Settings extends AppCompatActivity {

    Toolbar toolbar ;
    RadioGroup language ;
    RadioButton eng , arab ;
    Locale locale ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_settings);

        toolbar = (Toolbar) findViewById(R.id.settingtoolbar);
        setSupportActionBar(toolbar);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        set_lan_settings();



        findViewById(R.id.save_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Settings.this,Home.class);
                startActivity(intent);
                recreate();
            }
        });
    }

    private void saveLanguage(String lang) {

        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("key_lang", lang);
        editor.apply();

    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }

    public void set_lan_settings (){




        language = (RadioGroup) findViewById(R.id.lanradiogroup);
        eng = (RadioButton) findViewById(R.id.en);
        arab = (RadioButton) findViewById(R.id.ar);



        language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {

                    case R.id.en:

                        saveLanguage("en");
                        eng.setChecked(true);
                        arab.setChecked(false);
                        break;
                    case R.id.ar:

                        saveLanguage("ar");
                        arab.setChecked(true);
                        eng.setChecked(false);
                        break;
                }

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (getLangCode().equals("en")) {
            eng.setChecked(true);
            arab.setChecked(false);
        } else {
            arab.setChecked(true);
            eng.setChecked(false);
        }
    }

}
