package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.GcmRegistrationService;
import com.example.eslamsalah.newschool.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Login extends AppCompatActivity {

    Toolbar toolbar;
    EditText _emailText , _passwordText ;
    TextView _signupLink ;
    Button _loginButton;
    RequestQueue requestQueue ;

    String usertype , name , job ;
    int userid ;
    Locale locale ;

    BroadcastReceiver broadcastReceiver ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_login);

        setGcm();

        toolbar = findViewById(R.id.logintoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.login));
        toolbar.setTitleTextColor(Color.WHITE);

        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Home.class);
                startActivity(intent);
            }
        });
        _emailText = findViewById(R.id.input_email);
        _passwordText = findViewById(R.id.input_password);
        _loginButton = findViewById(R.id.btn_login);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
                 CheckUser();
                Intent intent = new Intent(Login.this,Home.class);
                startActivity(intent);

             }
        });

     }


    public void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        onLoginSuccess();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() ) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() ) {
            _passwordText.setError("between 1 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;


    }


    public void CheckUser(){
        String mail = _emailText.getText().toString();
        String pass = _passwordText.getText().toString();



        requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/Login/CheckUserss?id=" + Data.school_id + "&username=" + mail + "&password=" + pass, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            usertype = response.getString("UserType");
                            name = response.getString("Name");
                            job = response.getString("Job");
                            userid = response.getInt("UserID");


                         } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SharedPreferences pref = getSharedPreferences("info",MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = pref.edit();
                        editor1.putInt("UserID",userid);
                        editor1.putString("UserType",usertype);
                        editor1.putString("Name",name);
                        editor1.putString("Job",job);
                        editor1.apply();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Login.this, ""+error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }




        );


        requestQueue.add(jsonObjectRequest);

    }



    public void setGcm (){
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(GcmRegistrationService.REGISTRATION_SUCCESS)){

                    String token = intent.getStringExtra("token");
                    Toast.makeText(getApplicationContext(), "GCM Token :"+token, Toast.LENGTH_SHORT).show();
                } else if (intent.getAction().equals(GcmRegistrationService.REGISTRATION_ERROR)){

                    Toast.makeText(context, "GCM Error ", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //check status  of google_play service in device

        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS != resultcode){
            //check_type_of_error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)){
                Toast.makeText(this, "google play service not installed/enabled in this device", Toast.LENGTH_SHORT).show();
                //show notifications
                GooglePlayServicesUtil.showErrorNotification(resultcode,getApplicationContext());

            }else {
                Toast.makeText(this, "your device does not support for google play service", Toast.LENGTH_SHORT).show();
            }
        }else {
            //start service
            Intent intent = new Intent(this,GcmRegistrationService.class);
            startService(intent);
        }
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }








    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(GcmRegistrationService.REGISTRATION_SUCCESS));

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(GcmRegistrationService.REGISTRATION_ERROR));


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

//    public void Sign_In_Click(View view) {
//
//        _signupLink = (TextView) findViewById(R.id.link_signup);
//        Intent intent = new Intent(getApplicationContext(), Sign_up.class);
//        startActivity(intent);
//    }
}