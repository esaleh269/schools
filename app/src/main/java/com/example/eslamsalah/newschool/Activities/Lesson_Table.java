package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.example.eslamsalah.newschool.Activities.Results.type;

public class Lesson_Table extends AppCompatActivity {

    public static String Names[];
    public static String photo;
    Toolbar toolbar ;
    RequestQueue requestQueue ;
    Spinner spinner ;
    ArrayAdapter arrayAdapter ;
    Intent intent ;
    RecyclerView recyclerView ;
    ImageView table_Img ;
    Locale locale ;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_lesson__table);
        progressDialog = new ProgressDialog(Lesson_Table.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Names = new String[]{getResources().getString(R.string.fterm),getResources().getString(R.string.sterm)};
        intent = getIntent() ;
        table_Img = findViewById(R.id.table_img);
        toolbar = findViewById(R.id.lessontoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lesson Table");
        toolbar.setTitleTextColor(Color.WHITE);

        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
         set_Spinner ();
    }

    public void set_Spinner (){
        spinner = findViewById(R.id.lessons_spinner);
        arrayAdapter = new ArrayAdapter<String>(Lesson_Table.this,android.R.layout.simple_spinner_item,Names);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                type =   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
                String type_val = "";

                if (type.equals(getResources().getString(R.string.fterm))){

                    type_val = "First";
                }else if (type.equals(getResources().getString(R.string.sterm))){
                    type_val = "Second";
                }

                 GetLessons(type_val);

             }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
             }
        });
     }

    public void GetLessons (String type){
        requestQueue = Volley.newRequestQueue(this);
        int id = intent.getIntExtra("id",0);
        SharedPreferences preferences = getSharedPreferences("exinfo", MODE_PRIVATE);
        final String dom = preferences.getString("dom", null);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LessonsTable/GetStudentLessonsTable?id=" + Data.school_id + "&uid=" + id + "&term=" + type, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String img = response.getString("TablePic");
                            String xx = img.substring(2,img.length());
                            photo= dom+xx ;

                            Picasso.with(Lesson_Table.this).load(photo).into(table_Img);
//                            Bitmap imageBitmap = ((BitmapDrawable) table_Img.getDrawable()).getBitmap();
//                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
//                            imageDrawable.setCircular(true);
//                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
//                            table_Img.setImageDrawable(imageDrawable);

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonObjectRequest);


    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
