package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/14/2017.
 */

public class Children_Model {

    private String ChildName ;
    private int ChildId ;

    public String getChildName() {
        return ChildName;
    }

    public void setChildName(String childName) {
        ChildName = childName;
    }

    public int getChildId() {
        return ChildId;
    }

    public void setChildId(int childId) {
        ChildId = childId;
    }
}
