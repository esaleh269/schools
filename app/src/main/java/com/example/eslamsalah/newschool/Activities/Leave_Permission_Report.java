package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.LpRecycler;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.Model.LPModel;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Leave_Permission_Report extends AppCompatActivity {

    Toolbar toolbar ;
    DatePicker from , to ;
    Button ok ;
    String date1;
    String date2 ;
    ArrayList<LPModel> LP_List ;
    LpRecycler lp_recycler ;
    RecyclerView recyclerView ;
    LinearLayout linearLayout ;
    Locale locale ;
    ProgressDialog progressDialog ;


    RequestQueue requestQueue ;
    Intent intent ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_leave__permission__report);
        intent = getIntent();
        linearLayout=findViewById(R.id.lp_lin);
        toolbar = findViewById(R.id.leaveptoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.leavepermission));
        toolbar.setTitleTextColor(Color.WHITE);

        progressDialog = new ProgressDialog(Leave_Permission_Report.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");

        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SetDatePicker();
    }


    public void SetDatePicker(){

        from = findViewById(R.id.lpfromdp);
        to = findViewById(R.id.lptodp);
        ok = findViewById(R.id.showlp);


        final java.util.Calendar c = java.util.Calendar.getInstance();
        int year = c.get(java.util.Calendar.YEAR);
        int month = c.get(java.util.Calendar.MONTH);
        int day = c.get(java.util.Calendar.DAY_OF_MONTH);

        from.init(year, month, day, null);

        final java.util.Calendar c1 = java.util.Calendar.getInstance();
        int tyear = c1.get(java.util.Calendar.YEAR);
        int tmonth = c1.get(java.util.Calendar.MONTH);
        int tday = c1.get(java.util.Calendar.DAY_OF_MONTH);


        to.init(tyear, tmonth, tday, null);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();

                Integer year = from.getYear();
                Integer month = from.getMonth();
                Integer day = from.getDayOfMonth();

                date1 = (
                        String.valueOf(month + 1) + "-" + day + "-" +
                                year);

                Integer tyear = to.getYear();
                Integer tmonth = to.getMonth();
                Integer tday = to.getDayOfMonth();

                date2 = (
                        String.valueOf(tmonth + 1) + "-" + tday + "-" +
                                tyear );



                GetLeavePermission();
            }
        });



    }

    public void GetLeavePermission(){

        int StudentId = intent.getIntExtra("id",0);

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/GetStudentStudentleavePermission?id=" + Data.school_id + "&uid=" + StudentId + "&dtfrom=" + date1 + "&dtto=" + date2, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        LP_List = new ArrayList<>(response.length()+1);
                        LPModel lpModel1 = new LPModel();
                        lpModel1.setReason(getResources().getString(R.string.reason));
                        lpModel1.setDate(getResources().getString(R.string.date));
                        LP_List.add(lpModel1);
                        for (int i=0 ; i<response.length() ; i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                LPModel lpModel = new LPModel();
                                lpModel.setReason(jsonObject.getString("Reasone"));
                                lpModel.setDate(jsonObject.getString("PerDate"));
                                LP_List.add(lpModel);
                                progressDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (LP_List.size()==1){
                            Snackbar snackbar = Snackbar
                                    .make(linearLayout, "No Leave Permissions in This Range", Snackbar.LENGTH_LONG);

                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                        }else {
                            recyclerView = findViewById(R.id.lp_rec);
                            lp_recycler = new LpRecycler(LP_List,Leave_Permission_Report.this);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Leave_Permission_Report.this,LinearLayoutManager.VERTICAL,false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(lp_recycler);

                        }







                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Leave_Permission_Report.this, ""+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);





    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
