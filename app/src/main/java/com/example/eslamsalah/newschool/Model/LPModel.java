package com.example.eslamsalah.newschool.Model;



public class LPModel {

    private String reason ;
    private String date ;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
