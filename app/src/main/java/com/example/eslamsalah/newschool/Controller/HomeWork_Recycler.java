package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.HomeWork_Model;
import com.example.eslamsalah.newschool.R;

import org.json.JSONObject;

import java.util.ArrayList;



public class HomeWork_Recycler extends RecyclerView.Adapter<HomeWork_Recycler.Vholder> {

    private ArrayList<HomeWork_Model> HW_List ;
    private Context context ;
    private RequestQueue requestQueue ;

    public HomeWork_Recycler ( ArrayList<HomeWork_Model> HW_List,Context context){
        this.HW_List=HW_List;
        this.context=context;

    }

    @Override
    public Vholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homework_viewshape,parent,false);
        return new HomeWork_Recycler.Vholder(view);
    }

    @Override
    public void onBindViewHolder(final Vholder holder, final int position) {

        HomeWork_Model homeWorkModel = HW_List.get(position);
        holder.title.setText(homeWorkModel.getQtitle());
        holder.desc.setText(Html.fromHtml( homeWorkModel.getQdesc()));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Send_Request(position,holder);

             }
        });
      }

    private void Send_Request(int position, Vholder vholder){

        SharedPreferences preferences = context.getSharedPreferences("child", Context.MODE_PRIVATE);
        int userId = preferences.getInt("chid", 0);
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/homework/AnsureQuestion?id=11&uid="+userId+"&quid="+HW_List.get(position).getQid()
                        + "&answer=" + vholder.answer.getText(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//Status:Done
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, ""+error.toString(), Toast.LENGTH_LONG).show();
                      }
                }
        );
        requestQueue.add(jsonObjectRequest);

    }




    @Override
    public int getItemCount() {
        return HW_List.size();
    }

    public class Vholder extends RecyclerView.ViewHolder{

        TextView title , desc ;
        EditText answer ;
        Button button ;

        public Vholder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.qtitle);
            desc =  itemView.findViewById(R.id.qdesc);
            answer = itemView.findViewById(R.id.qanswer);
            button = itemView.findViewById(R.id.qsend);

        }

    }
}
