package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.EventModel;
import com.example.eslamsalah.newschool.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static java.util.Locale.ENGLISH;

public class Calendarr extends AppCompatActivity {

    Toolbar toolbar;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM-yyyy", Locale.getDefault());
    ActionBar actionBar;
    TextView textView;
    LinearLayout linearLayout;

    RequestQueue requestQueue;
    ArrayList<EventModel> Data ;
    CompactCalendarView compactCalendarView;
    Event ev1;
    ProgressDialog progressDialog ;
    Bundle b ;
    long[] mill_arr ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        textView = findViewById(R.id.event_decription);

        compactCalendarView = findViewById(R.id.compactcalendar_view);
        compactCalendarView.setLocale(TimeZone.getDefault(),ENGLISH);
        progressDialog = new ProgressDialog(Calendarr.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        linearLayout = findViewById(R.id.cal_lin);
        toolbar = findViewById(R.id.caltoolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(getResources().getString(R.string.calendar));
        toolbar.setTitleTextColor(Color.WHITE);

        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b = getIntent().getExtras();
        assert b != null;
        mill_arr = b.getLongArray("data");


        assert mill_arr != null;
        for (long aMill_arr : mill_arr) {
            ev1 = new Event(Color.GREEN, aMill_arr, "Some extra data that I want to store.");
            compactCalendarView.addEvent(ev1);
        }

         getData();

     }

    public void getData() {
        requestQueue = Volley.newRequestQueue(this);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/Events?id=" + com.example.eslamsalah.newschool.Model.Data.school_id, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Data =  new ArrayList<>(response.length());

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                EventModel eventModel = new EventModel();
                                eventModel.setName_en(jsonObject.getString("Name"));
                                eventModel.setName_ar(jsonObject.getString("NameAr"));
                                eventModel.setDesc_en(jsonObject.getString("Description"));
                                eventModel.setDesc_ar(jsonObject.getString("DescriptionAr"));
                                eventModel.setDate(jsonObject.getString("Date"));
                                eventModel.setTime(jsonObject.getString("EventTime"));
                                eventModel.setMillidate(jsonObject.getString("Totalmilliseconds"));
                                Data.add(eventModel);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                     progressDialog.dismiss();
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Calendarr.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                for (int i=0; i<Data.size() ; i++){
                    Locale loc_en = new Locale("en");
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd",loc_en);
                    String datee = format1.format(dateClicked);

                    //Toast.makeText(Calendarr.this, ""+datee, Toast.LENGTH_SHORT).show();


                    if (datee.compareTo(Data.get(i).getDate())==0){
                        textView.setText(Data.get(i).getDesc_ar());
                    }else {

                        Snackbar snackbar = Snackbar
                                .make(linearLayout, "No Events", Snackbar.LENGTH_LONG);

                        View sbView = snackbar.getView();
                        sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                        TextView ttextView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                        ttextView.setTextColor(Color.WHITE);
                        snackbar.show();

                        textView.setText("");

                    }
                }
             }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                actionBar.setTitle(simpleDateFormat.format(firstDayOfNewMonth));

            }
        });


    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }



}


