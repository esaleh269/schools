package com.example.eslamsalah.newschool.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.eslamsalah.newschool.R;

import java.util.Locale;

public class MessageDescription extends AppCompatActivity {

    TextView Title , Description , Date , To;
    android.support.v7.widget.Toolbar toolbar ;

    Intent intent ;
    Locale locale ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_message_description);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        intent = getIntent();
        toolbar = findViewById(R.id.msg_desc_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_message_description));
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MessageDescription.this,Messages.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        SetData();
    }

    public void SetData (){

        Title = findViewById(R.id.msg_desc_subj);
        Date = findViewById(R.id.msg_desc_date);
        Description = findViewById(R.id.msg_desc_description);
        To = findViewById(R.id.msg_desc_to);

        Title.setText(intent.getStringExtra("title"));
        Date.setText(intent.getStringExtra("date"));
        Description.setText(intent.getStringExtra("descrpt"));
        To.setText(intent.getStringExtra("ptname"));
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_reply:
                    Intent intent = new Intent(MessageDescription.this,ReplyMessage.class);
                    intent.putExtra("mytitle",Title.getText().toString());
                    intent.putExtra("sender",To.getText().toString());
                    intent.putExtra("original",Description.getText().toString());
                    startActivity(intent);
                    return true;

            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
