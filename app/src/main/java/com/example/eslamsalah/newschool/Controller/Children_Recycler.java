package com.example.eslamsalah.newschool.Controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eslamsalah.newschool.Model.Children_Model;
import com.example.eslamsalah.newschool.R;

import java.util.ArrayList;

/**
 * Created by eslam.salah on 12/14/2017.
 */

public class Children_Recycler extends RecyclerView.Adapter<Children_Recycler.VHOLDer> {

    ArrayList<Children_Model> Child_List ;
    Context context ;
    Child_click child_click ;


    public Children_Recycler ( ArrayList<Children_Model> Child_List1,Context context1){
        Child_List=Child_List1 ;
        context = context1 ;
    }

    @Override
    public VHOLDer onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.childviewshape,parent,false);
        return new Children_Recycler.VHOLDer(view);
    }

    public void SetListner(Child_click click){
        this.child_click=click ;
    }

    @Override
    public void onBindViewHolder(VHOLDer holder, final int position) {

        Children_Model childrenModel = Child_List.get(position);
        holder.Name.setText(childrenModel.getChildName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                child_click.ChooseChild(position);
            }
        });
     }

    @Override
    public int getItemCount() {
        return Child_List.size();
    }

    public class VHOLDer extends RecyclerView.ViewHolder{

        TextView Name , Id ;
        CardView cardView ;

        public VHOLDer(View itemView) {
            super(itemView);

            Name = itemView.findViewById(R.id.childname);
            cardView=itemView.findViewById(R.id.childcard);


        }
    }
    public interface Child_click {

        public void ChooseChild(int position);
    }
}
