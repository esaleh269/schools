package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/20/2017.
 */

public class Message_Model {

    private String title  ;
    private String desc  ;
    private String date  ;
    private String state ;
    private String id ;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
