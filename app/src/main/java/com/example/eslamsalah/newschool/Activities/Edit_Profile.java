package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class Edit_Profile extends AppCompatActivity {

    Toolbar toolbar ;
    EditText Uname,Password ,Email , Phone ;
    RequestQueue requestQueue ;
    Intent intent ;
    LinearLayout linearLayout ;
    Locale locale ;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_edit__profile);

        linearLayout =findViewById(R.id.editlinear);
        toolbar = findViewById(R.id.edittoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        toolbar.setTitleTextColor(Color.WHITE);
        progressDialog = new ProgressDialog(Edit_Profile.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        intent = getIntent();
        setEdittext();

        findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Edit_profile();
                Snackbar snackbar = Snackbar
                        .make(linearLayout, "Updated", Snackbar.LENGTH_LONG);

                View sbView = snackbar.getView();
                sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                 snackbar.show();
            }
        });


    }

    public void setEdittext (){

        int id = intent.getIntExtra("id", 0);

        Uname =   findViewById(R.id.username);
        Password =   findViewById(R.id.password);
        Email  =   findViewById(R.id.email);
        Phone =   findViewById(R.id.mobile);
        requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/GetUserInfo?id=15&uid=" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            Uname.setText( response.getString("UserName"));
                            Password.setText( response.getString("PassWord"));
                            Phone.setText( response.getString("Mobile"));
                            Email.setText( response.getString("Email"));
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(Edit_Profile.this, "" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
         );
        requestQueue.add(jsonObjectRequest);

    }

    public void Edit_profile (){

        int userid = intent.getIntExtra("id",0);
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/News/EditProfile?id=" + Data.school_id + "&uid=" + userid + "&username=" + Uname.getText().toString() +
                        "&pass=" + Password.getText().toString() + "&Mobile=" + Phone.getText().toString() + "&Email=" + Email.getText().toString(), null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonObjectRequest);
    }
    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
