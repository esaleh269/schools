package com.example.eslamsalah.newschool.Model;

/**
 * Created by eslam.salah on 12/14/2017.
 */

public class Ch_Act_Model {
    private int img ;
    private String name ;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
