package com.example.eslamsalah.newschool.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eslamsalah.newschool.Controller.Absence_Recycler;
import com.example.eslamsalah.newschool.Model.Absence_Model;
import com.example.eslamsalah.newschool.Model.Data;
import com.example.eslamsalah.newschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Absence extends AppCompatActivity {
    DatePicker from , to ;
    Button ok ;
    String date1;
    String date2 ;

    RequestQueue requestQueue ;
    Intent intent ;

    ArrayList<Absence_Model> Abs_List ;
    Absence_Recycler absence_recycler ;
    RecyclerView recyclerView ;
    LinearLayout linearLayout ;
    ProgressDialog progressDialog ;
     android.support.v7.widget.Toolbar toolbar ;
     Locale locale ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLanguage();
        setContentView(R.layout.activity_absence);


        intent = getIntent();
        linearLayout = findViewById(R.id.abs_lin);
        toolbar = findViewById(R.id.abstoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.absence));
        toolbar.setTitleTextColor(Color.WHITE);
        if (getLangCode().equals("en")){
            toolbar.setNavigationIcon(R.drawable.arrow16);
        }else {
            toolbar.setNavigationIcon(R.drawable.arrow162);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SetDatePicker();
    }

    public void SetDatePicker(){

        from = findViewById(R.id.fromdp);
        to = findViewById(R.id.todp);
        ok = findViewById(R.id.showabsence);


        final java.util.Calendar c = java.util.Calendar.getInstance();
        int year = c.get(java.util.Calendar.YEAR);
        int month = c.get(java.util.Calendar.MONTH);
        int day = c.get(java.util.Calendar.DAY_OF_MONTH);

        from.init(year, month, day, null);

        final java.util.Calendar c1 = java.util.Calendar.getInstance();
        int tyear = c1.get(java.util.Calendar.YEAR);
        int tmonth = c1.get(java.util.Calendar.MONTH);
        int tday = c1.get(java.util.Calendar.DAY_OF_MONTH);


        to.init(tyear, tmonth, tday, null);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(Absence.this,
                        R.style.Theme_AppCompat_DayNight_Dialog);
                progressDialog.setIndeterminate(false);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                Integer year = from.getYear();
                Integer month = from.getMonth();
                Integer day = from.getDayOfMonth();

                date1 = (
                        String.valueOf(month + 1) + "-" + day + "-" +
                                year);

                Integer tyear = to.getYear();
                Integer tmonth = to.getMonth();
                Integer tday = to.getDayOfMonth();

                date2 = (
                        String.valueOf(tmonth + 1) + "-" + tday + "-" +
                                tyear );


                getAbsence();
            }
        });



    }

    public void getAbsence (){

        int StudentId = intent.getIntExtra("id",0);

        requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                "http://epi.pioneers-solutions.org/api/LeavePermisionReport/GetAbsence?id=" + Data.school_id + "&uid=" + StudentId + "&dtfrom=" + date1 + "&dtto=" + date2, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Abs_List = new ArrayList<>(response.length());
                        for (int i=0 ; i<response.length() ; i++){
                            try {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                Absence_Model absence_model = new Absence_Model();
                                absence_model.setDate(jsonObject.getString("AbsDate"));
                                Abs_List.add(absence_model);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        recyclerView = findViewById(R.id.absence_rec);
                        absence_recycler = new Absence_Recycler(Abs_List,Absence.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Absence.this,LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(absence_recycler);
                        progressDialog.dismiss();


                        if (Abs_List.isEmpty()){
                            Snackbar snackbar = Snackbar
                                    .make(linearLayout, "No Absence in This Range", Snackbar.LENGTH_LONG);

                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(getResources().getColor(R.color.c1));
                            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Absence.this, ""+error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);

    }
    private void loadLanguage() {

        locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private String getLangCode() {
        SharedPreferences preferences = getSharedPreferences("lang", MODE_PRIVATE);
        String langCode = preferences.getString("key_lang", "en");
        return langCode;
    }
}
